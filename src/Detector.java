import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Detector {

  public static void main(String[] args) throws IOException {
    Scanner regex = null, text = null;
    try {
      regex = new Scanner(new FileReader(args[0]));
      text = new Scanner(new FileReader(args[1]));
      String s = text.useDelimiter("\\Z").next();
      while (regex.hasNext()) {
        Pattern pattern = Pattern.compile(regex.next());
        Matcher matcher = pattern.matcher(s);
        int i = 0;
        while (matcher.find()) i++;
        System.out.println(pattern+" "+i); 
      }		
    } finally {
      if (regex != null) regex.close();
      if (text != null) text.close();
    } 
  }
}
